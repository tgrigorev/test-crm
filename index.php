<?php
require('backend/library/Autoload.php');
$config = require('backend/config.php');

$app = new backend\library\Site();
$app->run($config);
