<?php use \backend\library\Site; ?>
<div class="register">
    <form method="post" enctype="multipart/form-data">
    <table id="register">
        <tr>
            <td><?=$oUser->label()['FIO']?>:</td>
            <td><input type="text" placeholder="Петров Федор Иванович" name="register[FIO]" value="<?=$oUser->aFields['FIO']?>">
            </td>
            <td><?=$oUser->aErrors['FIO']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Login']?>:</td>
            <td><input type="text" name="register[Login]" value="<?=$oUser->aFields['Login']?>"></td>
            <td><?=$oUser->aErrors['Login']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Password']?>:</td>
            <td><input type="password" name="register[Password]" value="<?=empty($oUser->aData)?
                    $oUser->aFields['Password']:'';?>"></td>
            <td><?=$oUser->aErrors['Password']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['PasswordConfirm']?>:</td>
            <td><input type="password" name="register[PasswordConfirm]" value="<?=empty($oUser->aData)?
                    $oUser->aFields['PasswordConfirm']:'';?>">

            </td>
            <td><?=$oUser->aErrors['PasswordConfirm']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['CountryId']?>:</td>
            <td class="position">
                <input type="text" placeholder="Россия" autocomplete="off" name="register[CountryId]" id="country"
                       value="<?=$oUser->getPositionName($oUser->aFields['CountryId'])?>">
                <div class="position_result" id="country_result"></div>
            </td>
            <td><?=$oUser->aErrors['CountryId']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['CityId']?>:</td>
            <td class="position"><input type="text" placeholder="Симферополь" autocomplete="off" name="register[CityId]" id="city"
                       value="<?=$oUser->getPositionName($oUser->aFields['CityId'])?>">
                <div class="position_result" id="city_result"></div>
            </td>
            <td><?=$oUser->aErrors['CityId']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Address']?>:</td>
            <td><input type="text" placeholder="пр.Кирова 36/5" name="register[Address]" value="<?=$oUser->aFields['Address']?>"></td>
            <td><?=$oUser->aErrors['Address']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Birthday']?>:</td>
            <td><?php $aBirthday = explode('-',$oUser->aFields['Birthday']); ?>
                <input class="day"  type="text" name="register[Birthday][Day]" placeholder="5" value="<?=$aBirthday['2']?>">
                <select name="register[Birthday][Month]" class="month">
                    <option value="">Месяц</option>
                    <?php foreach($oUser->month() as $iMonth => $sMonth):?>
                        <option value="<?=$iMonth?>" <?=$iMonth == $aBirthday['1']?'selected':''?>><?=$sMonth?></option>
                    <?php endforeach; ?>
                </select>
                <input  class="year" type="text" name="register[Birthday][Year]" placeholder="1984" value="<?=$aBirthday['0']?>"></td>
            <td><?=$oUser->aErrors['Birthday']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Gender']?>:</td>
            <td><select name="register[Gender]">
                    <option value=""></option>
                    <?php foreach($oUser->gender() as $sGender => $sName): ?>
                        <option value="<?= $sGender;?>" <?=$oUser->aFields['Gender']==$sGender?'selected':''?>><?= $sName;?></option>
                    <?php endforeach; ?>
                </select></td>
            <td><?=$oUser->aErrors['Gender']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Status']?>:</td>
            <td><select name="register[Status]">
                    <option value=""></option>
                    <?php foreach($oUser->status() as $sStatus => $sName): ?>
                        <option value="<?= $sStatus;?>" <?=$oUser->aFields['Status']==$sStatus?'selected':''?>><?= $sName;?></option>
                    <?php endforeach; ?>
                </select></td>
            <td><?=$oUser->aErrors['Status']?></td>
        </tr>
        <?php if(Site::$data['user']['role'] == 'admin'): ?>
        <tr>
            <td><?=$oUser->label()['Role']?>:</td>
            <td><select name="register[Role]">
                    <option value=""></option>
                    <?php foreach($oUser->roles() as $sStatus => $sName): ?>
                        <option value="<?= $sStatus;?>" <?=$oUser->aFields['Role']==$sStatus?'selected':''?>><?= $sName;
                            ?></option>
                    <?php endforeach; ?>
                </select></td>
            <td><?=$oUser->aErrors['Role']?></td>
        </tr>
        <?php endif; ?>
        <tr>
            <td><?=$oUser->label()['Other']?>:</td>
            <td><textarea class="about" placeholder="Люблю спорт, не курю, ответственный и незаменимый работник..."
                          name="register[Other]"><?=$oUser->aFields['Other']?></textarea></td>
            <td><?=$oUser->aErrors['Other']?></td>
        </tr>
        <tr>
            <td><?=$oPhone->label()['Phone']?>:</td>
            <td><input type="text" name="register[Phone][Data]" value="<?=$oPhone->aFields['Data']?>"></td>
            <td><?=$oPhone->aErrors['Data']?></td>
        </tr>
        <tr>
            <td><?=$oEmail->label()['Email']?>:</td>
            <td><input type="text" name="register[Email][Data]" value="<?=$oEmail->aFields['Data']?>"></td>
            <td><?=$oEmail->aErrors['Data']?></td>
        </tr>
    </table>
        <input type="submit" value="Отправить">
    </form>
</div>

<?php $this->js[] = 'position.js'; $this->join('js');?>