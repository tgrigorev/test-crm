<div class="info">
    <table class="info_text" cellspacing="0">
        <tr>
            <td><?=$oUser->label()['FIO']?>:</td>
            <td><?=$oUser->aData['FIO']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Gender']?>:</td>
            <td><?=$oUser->gender()[$oUser->aData['Gender']]?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Status']?>:</td>
            <td><?=$oUser->status()[$oUser->aData['Status']]?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Birthday']?>:</td>
            <td><?=$oUser->aData['Birthday']?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['CountryId']?>:</td>
            <td><?=$oUser->getPositionName($oUser->aData['CountryId'])?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['CityId']?>:</td>
            <td><?=$oUser->getPositionName($oUser->aData['CityId'])?></td>
        </tr>
        <tr>
            <td><?=$oUser->label()['Address']?>:</td>
            <td><?=$oUser->aData['Address']?></td>
        </tr>
        <tr>
            <?php $oContact = $oUser->getContact(['Type'=>'email']); ?>
            <td><?=$oContact->label()['Email']?>:</td>
            <td><?=$oContact->aData['Data']?></td>
        </tr>
        <tr>
            <?php $oContact = $oUser->getContact(['Type'=>'phone']); ?>
            <td><?=$oContact->label()['Phone']?>:</td>
            <td><?=$oContact->aData['Data']?></td>
        </tr>
    </table>
</div>
<div>

</div>