<div class="add_client"><a href="/cabinet/register"> + Добавить клиента</a></div>
<table class="clients" cellspacing="0">
    <tr>
        <td></td>
        <td><?= $oUsers->label()['FIO']?></td>
        <td><?= $oUsers->label()['CityId']?></td>
        <td><?= $oContact->label()['Phone']?></td>
        <td><?= $oContact->label()['Email']?></td>
        <td></td>
    </tr>
    <?php if(!empty($oUsers->aData)) foreach($oUsers->aData as $iKey => $aUser):?>
        <tr>
            <td><?=$aUser['Id']?></td>
            <td><?= $aUser['FIO']?></td>
            <td><?= $aUser['city']?></td>
            <td><?= $aUser['phone']?></td>
            <td><?= $aUser['email']?></td>
            <td><a href="<?= $this->url('/cabinet/edit',['id'=>$aUser['Id']])?>">
                    <img src="/frontend/images/design/edit.png" alt="Редактирование"></a>
                <a href="<?= $this->url('/cabinet/delete',['id'=>$aUser['Id']])?>"
                    onclick="if(!confirm('Вы уверены, что хотите удалить этого клиента?'))return false;">
                    <img src="/frontend/images/design/del.png" alt="Удаление"></a></td>
        </tr>
    <?php endforeach; ?>

</table>
<div class="pagination">
    <?php if($oUsers->aPagination['count'] &&($iCount =ceil($oUsers->aPagination['count']/$oUsers->aPagination['on_page']))>1) :
        for($i=0;$i<$iCount;$i++):?>
        <a href="?page=<?=$i?>" class="page <?= $i == $oUsers->aPagination['page']?'active':'' ?>"><?=($i+1)?></a>
    <?php endfor; endif; ?>
</div>
