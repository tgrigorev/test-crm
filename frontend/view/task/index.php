<div class="add_client"><a href="/task/add"> + Добавить задачу</a></div>
<table class="clients" cellspacing="0">
    <tr>
        <td><?= $oTask->label()['ProjectId']?></td>
        <td><?= $oTask->label()['AuthorId']?></td>
        <td><?= $oTask->label()['UserId']?></td>
        <td><?= $oTask->label()['Status']?></td>
        <td><?= $oTask->label()['Name']?></td>
        <td></td>
    </tr>
    <?php if(!empty($oTask->aData)) foreach($oTask->aData as $iKey => $aTask):?>
        <tr>
            <td><?=$aTask['ProjectId']?></td>
            <td><?= $aTask['AuthorId']?></td>
            <td><?= $aTask['UserId']?></td>
            <td><?= $aTask['Status']?></td>
            <td><?= $aTask['Name']?></td>
            <td><a href="<?= $this->url('/cabinet/edit',['id'=>$aTask['Id']])?>">
                    <img src="/frontend/images/design/edit.png" alt="Редактирование"></a>
                <a href="<?= $this->url('/cabinet/delete',['id'=>$aTask['Id']])?>"
                   onclick="if(!confirm('Вы уверены, что хотите удалить, этого клиента?'))return false;">
                    <img src="/frontend/images/design/del.png" alt="Удаление"></a></td>
        </tr>
    <?php endforeach; ?>

</table>
<div class="pagination">
    <?php if($oTask->aPagination['count'] &&($iCount =ceil($oTask->aPagination['count']/$oTask->aPagination['on_page']))>1) :
        for($i=0;$i<$iCount;$i++):?>
            <a href="?page=<?=$i?>" class="page <?= $i == $oTask->aPagination['page']?'active':'' ?>"><?=($i+1)?></a>
        <?php endfor; endif; ?>
</div>
