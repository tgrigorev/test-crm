<div class="register">
    <form method="post" enctype="multipart/form-data">
        <table id="register">
            <tr>
                <td><?=$oTask->label()['ProjectId']?>:</td>
                <td><select name="register[ProjectId]">
                        <option value=""></option>
                        <?php foreach([] as $sStatus => $sName): ?>
                            <option value="<?= $sStatus;?>" <?=$oTask->aFields['ProjectId']==$sStatus?'selected':''?>><?= $sName;?></option>
                        <?php endforeach; ?>
                    </select></td>
                <td><?=$oTask->aErrors['ProjectId']?></td>
            </tr>
            <tr>
                <td><?=$oTask->label()['AuthorId']?>:</td>
                <td><select name="register[AuthorId]">
                        <option value=""></option>
                        <?php foreach([] as $sStatus => $sName): ?>
                            <option value="<?= $sStatus;?>" <?=$oTask->aFields['AuthorId']==$sStatus?'selected':''?>><?= $sName;?></option>
                        <?php endforeach; ?>
                    </select></td>
                <td><?=$oTask->aErrors['AuthorId']?></td>
            </tr>
            <tr>
                <td><?=$oTask->label()['UserId']?>:</td>
                <td><select name="register[UserId]">
                        <option value=""></option>
                        <?php foreach([] as $sStatus => $sName): ?>
                            <option value="<?= $sStatus;?>" <?=$oTask->aFields['UserId']==$sStatus?'selected':''?>><?= $sName;?></option>
                        <?php endforeach; ?>
                    </select></td>
                <td><?=$oTask->aErrors['UserId']?></td>
            </tr>
            <tr>
                <td><?=$oTask->label()['Status']?>:</td>
                <td><select name="register[Status]">
                        <option value=""></option>
                        <?php foreach([] as $sStatus => $sName): ?>
                            <option value="<?= $sStatus;?>" <?=$oTask->aFields['Status']==$sStatus?'selected':''?>><?= $sName;?></option>
                        <?php endforeach; ?>
                    </select></td>
                <td><?=$oTask->aErrors['Status']?></td>
            </tr>

            <tr>
                <td><?=$oTask->label()['Name']?>:</td>
                <td><input type="text" placeholder="1" name="register[Name]" value="<?=$oTask->aFields['Name']?>">
                </td>
                <td><?=$oTask->aErrors['Name']?></td>
            </tr>
            <tr>
                <td><?=$oTask->label()['Description']?>:</td>
                <td><textarea class="about" placeholder="Ведение бизнеса крупного предприятия"
                              name="register[Description]"><?=$oTask->aFields['Description']?></textarea></td>
                <td><?=$oTask->aErrors['Description']?></td>
            </tr>
        </table>
        <input type="submit" value="Отправить">
    </form>
</div>