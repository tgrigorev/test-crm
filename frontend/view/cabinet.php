<?php
use backend\library\Site;

$aSecondMenu =  empty(Site::$data['user'])?[ //Разделы доступные всем зарегистрированым пользователям
    ]:array_merge([
        'Мой кабинет'=>$this->url('/cabinet'),
        'Настройки'=>$this->url('/cabinet/edit',['id'=>Site::$data['user']['id']]),
    ],((Site::$data['user']['role'] == 'admin')?[ //Разделы админа
        'Клиенты'=>$this->url('/cabinet/clients'),
        'Рабочий процесс' =>false,
        'Проекты' => $this->url('/project'),
        'Архив' => $this->url('/project/archive'),
    ]:[]),((Site::$data['user']['role'] == 'client')?[ //Разделы админа
        'Рабочий процесс' =>false,
        'Проекты' => $this->url('/project'),
        'Архив' => $this->url('/project/archive'),
    ]:[]));
?>
<div class="menu">
    <?php //Разделы доступные всем зарегистрированым пользователям
    $aMenu = empty(Site::$data['user'])?[
        'Вход'=>$this->url('/login'),
        'Регистрация'=>$this->url('/register'),
    ]:[
        'Выход'=>'/logout',
    ];
    ?>
    <div class="menu_block">
        <?php foreach($aMenu as $sName => $sUrl):?>
            <div class="menu_part"><a href="<?= $sUrl ?>"><?= $sName ?></a></div>
        <?php endforeach;?>
    </div>
</div>
    <div class="content">

    <div class="block_menu">
        <?php foreach($aSecondMenu as $sName => $sUrl):?>
            <?php if($sUrl): ?><div class="block_menu_part <?=  Site::$data['url']==$sUrl?'active':''?>">
                <a href="<?= $sUrl ?>"><?= $sName ?></a></div>
            <?php else: ?><div class="block_menu_partition"><?= $sName ?></div><?php endif; ?>
        <?php endforeach;?>
    </div>
    <div  class="block_content">
        <?= $content ?>
    </div>
</div>