<?php
    //$this->js[] = 'register.js';
?>
<html>
    <head>
        <title>@ TEST @</title>
        <?= $this->join('css'); ?>
    </head>
    <body>
        <noscript>
            <div class="black"></div>
            <div class="black_message">Для использования всех преимуществ системы,
                <br> вам необходимо включить javascript!</div>
        </noscript>
        <?= $content; ?>

        <?= $this->join('js'); ?>
    </body>
</html>