<div class="register">
    <form method="post" enctype="multipart/form-data">
        <table id="register">
            <tr>
                <td><?=$oProject->label()['Name']?>:</td>
                <td><input type="text" placeholder="Митис" name="register[Name]" value="<?=$oProject->aFields['Name']?>">
                </td>
                <td><?=$oProject->aErrors['Name']?></td>
            </tr>
            <tr>
                <td><?=$oProject->label()['Budget']?>:</td>
                <td><input type="text" placeholder="1" name="register[Budget]" value="<?=$oProject->aFields['Budget']?>">
                </td>
                <td><?=$oProject->aErrors['Budget']?></td>
            </tr>

            <tr>
                <td><?=$oProject->label()['Description']?>:</td>
                <td><textarea class="about" placeholder="Ведение бизнеса крупного предприятия"
                              name="register[Description]"><?=$oProject->aFields['Description']?></textarea></td>
                <td><?=$oProject->aErrors['Description']?></td>
            </tr>
        </table>
        <input type="submit" value="Отправить">
    </form>
</div>