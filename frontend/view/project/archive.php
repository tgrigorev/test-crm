<table class="clients" cellspacing="0">
    <tr>
        <td></td>
        <td><?=$oProject->label()['Name']?></td>
        <td><?=$oProject->label()['Budget']?></td>
        <td></td>
    </tr>
    <?php if(!empty($oProject->aData)) foreach($oProject->aData as $iKey => $aProject):?>
        <tr>
            <td><?=$aProject['Id']?></td>
            <td><?=$aProject['Name']?></td>
            <td><?=$aProject['Budget']?></td>
            <td><a href="<?= $this->url('/project/edit',['id'=>$aProject['Id']])?>">
                    <img src="/frontend/images/design/edit.png" alt="Редактирование"></a>
                <?php if(!$aProject['Finish']):?>
                    <a href="<?= $this->url('/project/done',['id'=>$aProject['Id']])?>"
                       onclick="if(!confirm('Вы уверены, что проект завершён?'))return false;">
                        <img src="/frontend/images/design/ok.png" alt="Редактирование"></a>
                <?php endif;?>
                <a href="<?= $this->url('/project/delete',['id'=>$aProject['Id']])?>"
                   onclick="if(!confirm('Вы уверены, что хотите удалить этот проект?'))return false;">
                    <img src="/frontend/images/design/del.png" alt="Удаление"></a></td>
        </tr>
    <?php endforeach; ?>
</table>
<div class="pagination">
    <?php if($oProject->aPagination['count'] &&($iCount =ceil($oProject->aPagination['count']/$oProject->aPagination['on_page']))>1) :
        for($i=0;$i<$iCount;$i++):?>
            <a href="?page=<?=$i?>" class="page <?= $i == $oProject->aPagination['page']?'active':'' ?>"><?=($i+1)?></a>
        <?php endfor; endif; ?>
</div>
