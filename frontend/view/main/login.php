<style>body{background: url("/frontend/images/design/login_page_bg.jpg")}</style>
<div class="login">
    <div class="login_logo">

    </div>
    <form method="post" id="form_lost">
        <div class="login_name">Восстановление пароля</div>
        <div class="field">
            <div class="field_name"><?=$oUser->label()['Login']?>:</div>
            <div class="field_data"><input type="text" id="login_lost" name="login[Login]" value="<?=$oUser->aData['Login']?>"></div>
            <?=$oUser->aErrors['Login']?>
        </div>

        <div class="field button">
            <div class="lost_password" id="lost_none">Войти</div>
            <input type="submit" value="Восстановить">
        </div>
        <div class="login_error">
            <?php $aErrors = [
                'login_error'=>'Введенный логин отсутствует в базе!',
                'no_login_error'=>'Заполните поле Логин!'
            ];?>
            <?php foreach($aErrors as $sError => $sName):?>
                <div class="errors_lost <?= $oUser->aErrors['main'] == $sError?'':'hide';?>" id="<?=$sError?>_lost">
                    <?=$sName?>
                </div>
            <?php endforeach; ?>
            <?= $oUser->aErrors['main']; ?>
        </div>
    </form>
    <form method="post" id="form_login">
        <div class="login_name">Вход</div>
        <div class="field">
            <div class="field_name"><?=$oUser->label()['Login']?>:</div>
            <div class="field_data"><input type="text" id="login" name="login[Login]" value="<?=$oUser->aData['Login']?>"></div>
            <?=$oUser->aErrors['Login']?>
        </div>

        <div class="field <?= !empty($oUser->aData['Login'])?'':'hide'; ?>" id="password_block">
            <div class="field_name"><?=$oUser->label()['Password']?>:</div>
            <div class="field_data"><input type="password" id="password" name="login[Password]" value=""></div>
            <?=$oUser->aErrors['Password']?>
        </div>


        <div class="field button">
            <div class="lost_password" id="lost">Забыли пароль?</div>
            <input type="submit" id="button" value="Войти">
        </div>
            <div class="login_error">
                <?php $aErrors = [
                    'login_error'=>'Введенный логин отсутствует в базе!',
                    'password_error'=>'Вы допустили ошибку в поле Пароль!',
                    'no_login_error'=>'Вы не заполнили поле Логин!'
                ];?>
                <?php foreach($aErrors as $sError => $sName):?>
                    <div class="errors <?= $oUser->aErrors['main'] == $sError?'':'hide';?>" id="<?=$sError?>">
                        <?=$sName?>
                    </div>
                <?php endforeach; ?>
                <?= $oUser->aErrors['main']; ?>
            </div>
    </form>
</div>
<?php $this->js[] = 'login.js'; $this->join('js');?>