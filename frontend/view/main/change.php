<style>body{background: url("/frontend/images/design/login_page_bg.jpg")}</style>
<div class="login">
    <div class="login_logo">

    </div>
    <form method="post" id="form_change">
        <div class="login_name">Изменение пароля</div>
        <div class="field" id="password_block">
            <div class="field_name"><?=$oUser->label()['Password']?>:</div>
            <div class="field_data"><input type="password" id="password" name="login[Password]" value=""></div>
            <?=$oUser->aErrors['Password']?>
        </div>

        <div class="field" id="password_block">
            <div class="field_name"><?=$oUser->label()['PasswordConfirm']?>:</div>
            <div class="field_data"><input type="password" id="password_confirm" name="login[PasswordConfirm]" value=""></div>
            <?=$oUser->aErrors['PasswordConfirm']?>
        </div>


        <div class="field button">
            <input type="submit" id="button" value="Изменить">
        </div>
        <div class="login_error">
            <?php $aErrors = [
                'no_password_error'=>'Вы не заполнили поле пароль!',
                'no_password_confirm_error'=>'Вы не заполнили поле подтверждения пароля!',
                'password_no_confirm_error'=>'Пароли не совпадают!',
            ];?>
            <?php foreach($aErrors as $sError => $sName):?>
                <div class="errors <?= $oUser->aErrors['main'] == $sError?'':'hide';?>" id="<?=$sError?>">
                    <?=$sName?>
                </div>
            <?php endforeach; ?>
            <?= $oUser->aErrors['main']; ?>
        </div>
    </form>
</div>
<?php $this->js[] = 'change.js'; $this->join('js');?>