var bPass= false;
$('#form_login').addEventListener('submit',function(e){
    var xmlhttp = ajax();
    xmlhttp.open("POST", "/login", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState != 4) return;

        if (xmlhttp.status == 200) {
            aData = JSON.parse(xmlhttp.responseText);

            hide('errors');
            if(aData['auth']){
                document.location.href = '/cabinet';
            }else if(aData['pass']){
                bPass= true;
                $('#password_block').style.display = 'block';
            }else{
                $('#'+aData['error']).style.display = 'block';
            }
            console.log(xmlhttp.responseText);

        } else {
            handleError(xmlhttp.statusText)
        }
    }
        e.preventDefault();

    if(bPass)
        sPassword = "&Password="+$('#password').value;
            else sPassword = '';

        sRequest = "ajax=1&Login="+$('#login').value + sPassword;
        xmlhttp.send(sRequest);
    })

$('#form_lost').addEventListener('submit',function(e){
    var xmlhttp = ajax();
    xmlhttp.open("POST", "/lost", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState != 4) return;

        if (xmlhttp.status == 200) {
            aData = JSON.parse(xmlhttp.responseText);

            hide('errors_lost');
            if(aData['send']){
                console.log(xmlhttp.responseText);
                //document.location.href = '/login';
            }else{
                $('#'+aData['error']+'_lost').style.display = 'block';
            }
        } else {
            handleError(xmlhttp.statusText)
        }
    }
        e.preventDefault();

        sRequest = "ajax=1&Login="+$('#login_lost').value;
        xmlhttp.send(sRequest);
    })


$('#lost').addEventListener('click',function(e){
    $('#form_login').style.display = 'none';
    $('#form_lost').style.display = 'block';
})
$('#lost_none').addEventListener('click',function(e){
    console.log(1);
    $('#form_login').style.display = 'block';
    $('#form_lost').style.display = 'none';
})