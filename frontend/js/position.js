function position(e){
    var xmlhttp = ajax();
    xmlhttp.open("POST", "/cabinet/position", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState != 4) return;

        if (xmlhttp.status == 200) {
            var aResult = JSON.parse(xmlhttp.responseText);
            console.log(e.target.id);
            $('#'+e.target.id+'_result').innerHTML = '';
            $('#'+e.target.id+'_result').style.display = 'block';
            for(var i=0;i<aResult.length;i++){
                var div = document.createElement('div');
                div.className = e.target.id+'_result';
                div.innerHTML = aResult[i];
                $('#'+e.target.id+'_result').appendChild(div);
            }
        } else {
            //handleError(xmlhttp.statusText); // вызвать обработчик ошибки с текстом ответа
        }
    }
    if(e.target.id == 'city'){
        if($('#country').value)
            var sCountry = '&country='+$('#country').value;
        else{
            alert('Вам необходимо выбрать страну');
            return;
        }

    }
    else var sCountry = '';
    xmlhttp.send("ajax=1&name="+e.target.value+"&type="+e.target.id+sCountry);
}
$('#country').addEventListener('click',position)
$('#country').addEventListener('keyup',position)
$('#city').addEventListener('click',position)
$('#city').addEventListener('keyup',position)
document.addEventListener('click',function(e){
    if(e.target.className == 'country_result') $('#country').value = e.target.innerHTML;
    if(e.target.className == 'city_result') $('#city').value = e.target.innerHTML;
        $('#country_result').style.display = 'none';
        $('#country_result').innerHTML = '';
        $('#city_result').style.display = 'none';
        $('#city_result').innerHTML = '';
})
