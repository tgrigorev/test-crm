$('#form_change').addEventListener('submit',function(e){
    var xmlhttp = ajax();
    xmlhttp.open("POST", window.location, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState != 4) return;

        if (xmlhttp.status == 200) {

            console.log(xmlhttp.responseText);
            aData = JSON.parse(xmlhttp.responseText);

            hide('errors');
            if(aData['done']){
                document.location.href = '/login';
            }else{
                $('#'+aData['error']).style.display = 'block';
            }
        } else {
            console.log(xmlhttp.responseText);
            handleError(xmlhttp.statusText)
        }
    }
    e.preventDefault();

    sRequest = "ajax=1&Password="+$('#password').value+"&PasswordConfirm="+$('#password_confirm').value;
    xmlhttp.send(sRequest);
})