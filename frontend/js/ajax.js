function ajax(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function handleError(message) {
    alert("Ошибка: "+message)
}

function hide(className){
    var elements = document.getElementsByClassName(className);
    for(var i = 0, length = elements.length; i < length; i++) {
        elements[i].style.display = 'none';
    }
}

function $(data){
    switch (data[0]){
        case '.':
            return document.getElementsByClassName(data.substr(1));
        case '#':
            return document.getElementById(data.substr(1));
        default:
            return document.getElementsByTagName(data.substr(1));
    }
}


