-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 25 2015 г., 23:51
-- Версия сервера: 5.6.15-log
-- Версия PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Type` enum('phone','email','skype') NOT NULL,
  `Data` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Contact_user_UserId` (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Контакт' AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`Id`, `UserId`, `Type`, `Data`) VALUES
(5, 29, 'phone', '123123123123'),
(6, 29, 'email', 'great@mail.ru'),
(25, 33, 'phone', '12312312313'),
(26, 33, 'email', 'dfgdfgd@sdfdfsd.com'),
(29, 35, 'phone', '132234234'),
(30, 35, 'email', 'dfdsfsd@sdfsd.com'),
(31, 36, 'phone', '32423423424'),
(32, 36, 'email', 'dfdserersd@sdfsd.com'),
(33, 37, 'phone', '6546465546'),
(34, 37, 'email', '111fghgh@gdfgdf.com'),
(35, 38, 'phone', '123123123'),
(36, 38, 'email', 'fgdfgd@dfgdfg.com');

-- --------------------------------------------------------

--
-- Структура таблицы `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Страна',
  `CountryId` int(11) DEFAULT NULL COMMENT 'Регион',
  `RegionId` int(11) DEFAULT NULL COMMENT 'Город',
  `Type` enum('country','region','city') NOT NULL COMMENT 'Вид',
  `Name` varchar(50) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `position`
--

INSERT INTO `position` (`Id`, `CountryId`, `RegionId`, `Type`, `Name`) VALUES
(1, NULL, NULL, 'country', 'Россия'),
(2, 1, NULL, 'city', 'Симферополь'),
(3, 1, NULL, 'city', 'Ялта'),
(4, NULL, NULL, 'country', 'Nigeria'),
(5, 4, NULL, 'city', 'Macha Pikchu');

-- --------------------------------------------------------

--
-- Структура таблицы `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AuthtorId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL COMMENT 'Название',
  `Description` mediumtext NOT NULL COMMENT 'Описание',
  `Budget` decimal(12,0) NOT NULL COMMENT 'Бюджет',
  `Start` datetime NOT NULL COMMENT 'Начало',
  `Finish` datetime DEFAULT NULL COMMENT 'Окончание',
  PRIMARY KEY (`Id`),
  KEY `FK_project_user_Id` (`AuthtorId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Проект' AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryId` int(11) DEFAULT NULL COMMENT 'Страна',
  `RegionId` int(11) DEFAULT NULL COMMENT 'Регион',
  `CityId` int(11) DEFAULT NULL COMMENT 'Город',
  `Login` varchar(50) NOT NULL COMMENT 'Логин',
  `Password` varchar(100) NOT NULL COMMENT 'Пароль',
  `Role` enum('admin','client') NOT NULL DEFAULT 'client' COMMENT 'Роль',
  `FIO` varchar(50) NOT NULL COMMENT 'Отчество',
  `Gender` enum('man','woman') NOT NULL COMMENT 'Пол',
  `Status` enum('married','meet','divorced','free','find') NOT NULL COMMENT 'Статус',
  `Birthday` date NOT NULL COMMENT 'День рождения',
  `Address` varchar(255) NOT NULL COMMENT 'Адрес',
  `Other` mediumtext NOT NULL COMMENT 'О себе',
  `Register` datetime NOT NULL COMMENT 'Регистрация',
  PRIMARY KEY (`Id`),
  KEY `FK_user_position_PositionId` (`CountryId`),
  KEY `FK_user_position_PositionId2` (`CityId`),
  KEY `FK_user_position_PositionId3` (`RegionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Пользователь' AUTO_INCREMENT=39 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`Id`, `CountryId`, `RegionId`, `CityId`, `Login`, `Password`, `Role`, `FIO`, `Gender`, `Status`, `Birthday`, `Address`, `Other`, `Register`) VALUES
(29, 1, NULL, 2, '123', 'ac8de3b5b736fd627b42c91071c5c2a6ec963a89', 'admin', 'Тимур', 'man', 'married', '1990-01-02', 'улица 1', 'да дадад', '2015-06-22 08:25:41'),
(33, 1, NULL, 2, 'petr', 'ac8de3b5b736fd627b42c91071c5c2a6ec963a89', '', 'Петров Петр Петровичё', 'man', 'married', '1988-05-05', 'пр.Кирова 35/17', '', '0000-00-00 00:00:00'),
(35, 1, NULL, 3, 'fed', 'ac8de3b5b736fd627b42c91071c5c2a6ec963a89', 'client', 'Федоров Федор Федорович', 'man', 'free', '1997-08-12', 'пр.Кирова 12/1', '', '0000-00-00 00:00:00'),
(36, 1, NULL, 2, 'van', 'ac8de3b5b736fd627b42c91071c5c2a6ec963a89', 'client', 'Иванов Иван Иванович', 'man', 'divorced', '1984-02-04', 'пр.Кирова 35/17', '', '0000-00-00 00:00:00'),
(37, 4, NULL, 5, 'gen', 'ac8de3b5b736fd627b42c91071c5c2a6ec963a89', '', 'Евгенев Евгений Евгеневич', 'man', 'meet', '1985-09-06', 'ул. Ленина 5/4', '', '2015-06-23 14:54:24'),
(38, 4, NULL, 5, 'kos', 'ac8de3b5b736fd627b42c91071c5c2a6ec963a89', 'client', 'Константинов Константин Константинович', 'man', 'free', '1956-04-03', 'dgfsdfs', '', '2015-06-25 21:30:55');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `FK_Contact_user_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `FK_project_user_Id` FOREIGN KEY (`AuthtorId`) REFERENCES `user` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_position_PositionId` FOREIGN KEY (`CountryId`) REFERENCES `position` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_user_position_PositionId2` FOREIGN KEY (`CityId`) REFERENCES `position` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_user_position_PositionId3` FOREIGN KEY (`RegionId`) REFERENCES `position` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
