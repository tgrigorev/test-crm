<?php
/**
 * Created by PhpStorm.
 * User: Spirit Fire
 * Date: 17.06.15
 * Time: 17:25
 */

namespace backend\models;


class MessageModel {
    public function table(){
        return 'message';
    }

    public function rules(){
        return [
            'required'=>['AuthorId','RecipientId','Message'],
            'int'=>['AuthorId','RecipientId'],
            'string'=>['Message'],
            'fields'=>['AuthorId','RecipientId','Message','Send']
        ];
    }

    public function label(){
        $aData = [
            'ru' => [
                'UserId'=>'Пользователь',
                'Type'=>'Вид',
                'Data'=>'Контакт',
                'Phone'=>'Телефон',
                'Email'=>'Почта'],
            'en' => [
                'UserId'=>'User',
                'Type'=>'Type',
                'Data'=>'Data',
                'Phone'=>'Phone',
                'Email'=>'Email']
        ];

        return $aData[Site::$data['lang']];
    }
} 