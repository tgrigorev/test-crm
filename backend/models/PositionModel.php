<?php

namespace backend\models;

use backend\library\Model;
use backend\library\Site;

class PositionModel extends Model {

    public function table(){
        return 'position';
    }

    public function rules(){
        return [
            'required'=>['Type','Name'],
            'int'=>['CountryId','RegionId'],
            'enum'=>['Type'=>['country','region','city']],
            'fields'=>['CountryId','RegionId','Type','Name']
        ];
    }

    public function label(){

        $aData = [
            'ru' => [
                'CountryId'=>'Страна',
                'RegionId'=>'Регион',
                'Type'=>'Вид',
                'Name'=>'Название'],
            'en' => [
                'CountryId'=>'Country',
                'RegionId'=>'Region',
                'Type'=>'Type',
                'Name'=>'Name']
        ];

        return $aData[Site::$data['lang']];
    }


    public function getPosition($_aNames,$_sType){
        $aResults =[];
        $aCountry = [];

        if(!empty($_aNames[1])){
            $this->findByParams(['Name'=>$_aNames[1],'Type'=>'country']);
            if(!empty($this->aData)) $aCountry = ['CountryId'=>$this->aData['Id']];
        }

        $this->findAllByParams(array_merge(['Name'=>$_aNames[0],'Type'=>$_sType],$aCountry),
                ['limit'=>15,'type'=>'like']);

        if(!empty($this->aData)){
            foreach($this->aData as $aResult){
                $aResults[]=$aResult['Name'];
            }
        }

        return $aResults;
    }
}