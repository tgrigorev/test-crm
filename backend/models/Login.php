<?php

namespace backend\models;

use backend\library\Site;

class Login {

    public static function login($_sLogin,$_sPass,$_oUser){
        if($aoUser = $_oUser->findByParams(['Login'=>$_sLogin]))
        {
            if($aoUser->aData['Password'] === self::hash($_sPass))
            {
                session_start();
                $_SESSION['user'] = [
                    'data'=>date('Ymd'),
                    'id'=>$aoUser->aData['Id'],
                    'role' => $aoUser->aData['Role']];
                return $_SESSION['user'];
            }
        }
        return false;
    }

    public static function logout(){

        session_start();
        unset($_SESSION['user']);
        session_destroy();

        header('Location:/');
    }

    public static function check(){
        session_start();
        if(!empty($_SESSION['user']))
        {
            $_SESSION['user']['date'] = date('Ymd');

            return Site::$data['user'] = $_SESSION['user'];
        }
        return false;
    }

    public static function register($_oUser){
        session_start();
        $_SESSION['user'] = [
            'date'=>date('Ymd'),
            'id'=>$_oUser->aFields['Id'],
            'role' => $_oUser->aFields['Role']];
        return $_SESSION['user'];
    }

    public static function hash($_sPassword){
        return sha1($_sPassword.'test');
    }
} 