<?php

namespace backend\models;

use backend\library\Model;
use backend\library\Site;

class InfoModel extends Model {

    public function table(){
        return 'info';
    }

    public function rules(){
        return [
            'required'=>['UserId','Type','Start','Data'],
            'int'=>['UserId'],
            'date'=>['Start','Finish'],
            'string'=>['Data'],
            'fields'=>['UserId','Type','Start','Finish','Data']
        ];
    }

    public function label(){

        $aData = [
            'ru' => [
                'UserId'=>'User',
                'Type'=>'Вид',
                'Start'=>'Начало',
                'Finish'=>'Окончание',
                'Data'=>'Описание'
            ],
            'en' => [
                'UserId'=>'User',
                'Type'=>'Type',
                'Start'=>'Start',
                'Finish'=>'Finish',
                'Data'=>'Data'
            ],
        ];

        return $aData[Site::$data['lang']];
    }
} 