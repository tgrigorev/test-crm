<?php

namespace backend\models;

use backend\library\Model;
use backend\library\Site;

class UserModel extends Model{

    /**
     * Возвращает имя таблицы
     * @return string
     */

    public function table(){
        return 'user';
    }

    /**
     * Возвращает правил применяемые к данным в данной моделе
     * @return array
     */

    public function rules(){
        return [
            'required'=>['CountryId','CityId','Login','Password','Role','FIO','Gender','Status','Birthday',
                'Address'],
            'int'=>['CountryId','CityId'],
            'string'=>['FIO','Address','Password','Other'],
            'confirm'=>['Password','PasswordConfirm'],
            'date'=>['Birthday'],
            'uniqum'=>['Login'],
            'enum'=>[
                'Gender'=>['man','woman'],
                'Status'=>['married','meet','divorced','free','find']],
            'fields'=>['CountryId','CityId','Login','Password','FIO','Role','Gender','Status','Birthday',
                'Address','Other','Register']
        ];
    }

    /**
     * Настройка связей между таблицами
     * @return array
     */

    public function relation(){
        return [
            'country'=>['PositionModel',['Id','CountryId'],'Name'],
            'city'=>['PositionModel',['Id','CityId'],'Name'],
            'email'=>['ContactModel',['UserId','Id'],'Data',['Type'=>'email']],
            'phone'=>['ContactModel',['UserId','Id'],'Data',['Type'=>'phone']]
        ];
    }

    /**
     * Возвращает названия полей, так же настраиваеться перевод
     * @return string
     */

    public function label(){

        $aData = [
            'ru' => [
                'CountryId'=>'Страна',
                'RegionId'=>'Регион',
                'CityId'=>'Город',
                'Login'=>'Логин',
                'Password'=>'Пароль',
                'PasswordConfirm'=>'Подтверждение',
                'Role'=>'Роль',
                'FIO'=>'Ф.И.О.',
                'Gender'=>'Пол',
                'Status'=>'Семейное положение',
                'Birthday'=>'День рождения',
                'Address'=>'Адрес',
                'Other'=>'О себе'],
            'en' => [
                'CountryId'=>'Country',
                'RegionId'=>'Region',
                'CityId'=>'City',
                'Login'=>'Login',
                'Password'=>'Password',
                'PasswordConfirm'=>'Password Confirm',
                'Role'=>'Role',
                'FIO'=>'Full name',
                'Gender'=>'Gender',
                'Status'=>'Family status',
                'Birthday'=>'Birthday',
                'Address'=>'Address',
                'Other'=>'About'],
        ];

        return $aData[Site::$data['lang']];
    }

    /**
     * Возвращает названия месяцев, так же настраиваеться перевод
     * @return string
     */

    public function month(){
        $aMonth = ['ru'=>[
            '1'=>'Января',
            '2'=>'Февраля',
            '3'=>'Марта',
            '4'=>'Апреля',
            '5'=>'Мая',
            '6'=>'Июня',
            '7'=>'Июля',
            '8'=>'Августа',
            '9'=>'Сентября',
            '10'=>'Октября',
            '11'=>'Ноября',
            '12'=>'Декабря'
        ],
            'en'=>[
                '1'=>'January',
                '2'=>'February',
                '3'=>'March',
                '4'=>'April',
                '5'=>'May',
                '6'=>'June',
                '7'=>'July',
                '8'=>'August',
                '9'=>'September',
                '10'=>'October',
                '11'=>'November',
                '12'=>'December'
            ]];
        return $aMonth[Site::$data['lang']];
    }

    /**
     * Возвращает названия статуса отношений, так же настраиваеться перевод
     * @return string
     */

    public function status(){
        $aStatus = ['ru'=>[
            'married'=>'В браке',
            'meet'=>'Встречаюсь',
            'divorced'=>'В разводе',
            'free'=>'Свободен',
            'find'=>'В поиске'
        ],
            'en'=>[
                'married'=>'Married',
                'meet'=>'Meet',
                'divorced'=>'Divorced',
                'free'=>'Free',
                'find'=>'Find'
            ]];
        return $aStatus[Site::$data['lang']];
    }

    /**
     * Возвращает названия статуса отношений, так же настраиваеться перевод
     * @return string
     */

    public function roles(){
        $aStatus = ['ru'=>[
            'admin'=>'Админ',
            'client'=>'Клиент',
            'manager'=>'Управляющий',
            'coder'=>'Программист',
        ],
            'en'=>[
                'admin'=>'Admin',
                'client'=>'Client',
                'manager'=>'Manager',
                'coder'=>'Coder',
            ]];
        return $aStatus[Site::$data['lang']];
    }

    /**
     * Возвращает названия пола, так же настраиваеться перевод
     * @return string
     */

    public function gender(){
        $aGender = ['ru'=>[
            'man'=>'Мужчина',
            'woman'=>'Женщина'
        ],
            'en'=>[
                'man'=>'Man',
                'woman'=>'Woman'
            ]];
        return $aGender[Site::$data['lang']];
    }

    /**
     * Подготовка данных к проверке, блокирует повторное применение действий к данным
     */

    public function beforeValidate(){
        $oPosition = new PositionModel();

        $this->aFields['CountryId'] = $this->aFields['CountryId']>0?$this->aFields['CountryId']:
                $oPosition->findByParams(['Name'=>$this->aFields['CountryId']],['Id'])->aData['Id'];
        $this->aFields['CityId'] = $this->aFields['CityId']>0?$this->aFields['CityId']:
                $oPosition->findByParams(['Name'=>$this->aFields['CityId']],['Id'])->aData['Id'];

        if(empty($this->aData))
        {
            $this->aFields['Role'] = 'client';
            $this->aFields['Register'] = date('Y-m-d H:i:s');
        }

        parent::beforeValidate();
    }

    /**
     * Подгатавливает данные перед сохранением
     * @return NULL
     */

    public function beforeSave(){
        if(empty($this->aData)) $this->aFields['Password'] = Login::hash($this->aFields['Password']);
        parent::beforeSave();
    }

    /**
     * Возвращает Email пользователя
     */

    public function getContact($_aParam = ['Type'=>'email'], $_iId = 0){
        $oContact = new ContactModel();
        return $oContact->findByParams(array_merge($_aParam,['UserId'=>$_iId?:$this->aData['Id']]));
    }

    /**
     * Возвращает Название Страны или Города !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     */

    public function getPositionName($_iId){
        $oContact = new PositionModel();
        return $oContact->findByParams(['Id'=>$_iId],['Name'])->aData['Name'];
    }

    /**
     * Отсылает письмо с запросом на изменение пароля
     */

    public function sendMailChange($_sAddress,$_mData){

        $sMessage = 'На ваш аккаунт поступило заявление о смене пароля, если вы не отправляли запрос, '.
            'просьба проигнорировать это письмо. Для изменения пароля перейдите по ссылке ниже: <br>'.
            '<a href ="test.com/change?protect='.$_mData.'">Сменить пароль</a>';

        return $this->mail($_sAddress,'Восстановление пароля',$sMessage);
    }

    /**
     * Отсылает письмо с извещением о изменении пароля
     */

    public function sendMailChanged($_sAddress,$_mData){

        $sMessage = 'Ваш пароль изменен: <b>'.$_mData.'</b>';

        return $this->mail($_sAddress,'Пароль изменен',$sMessage);
    }

    /**
     * Отсылает письмо с извещением о регистрации в системе
     */

    public function sendMailRegister($_sAddress,$_mData){

        $sMessage = 'Вы были зарегестрированны в тестовой системе, ваш <br>логин: <b>'.$_mData['login'].'</b>'.
        '<br>пароль: <b>'.$_mData['password'].'</b>';

        return $this->mail($_sAddress,'Регистрация',$sMessage);
    }
}