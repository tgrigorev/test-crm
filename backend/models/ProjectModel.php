<?php

namespace backend\models;

use backend\library\Model;
use backend\library\Site;

class ProjectModel extends Model {

    /**
     * Возвращает имя таблицы
     * @return string
     */

    public function table(){
        return 'project';
    }

    /**
     * Возвращает правил применяемые к данным в данной моделе
     * @return array
     */

    public function rules(){
        return [
            'required'=>['Name','Description','Start','Budget'],
            'int'=>['Budget'],
            'string'=>['Name','Description'],
            'fields'=>['Name','AuthorId','Description','Budget','Start','Finish']
        ];
    }

    /**
     * Настройка связей между таблицами
     * @return array
     */

    public function relation(){
        return [
            //'country'=>['PositionModel',['Id','CountryId'],'Name'],
            //'phone'=>['ContactModel',['UserId','Id'],'Data',['Type'=>'phone']]
        ];
    }

    /**
     * Возвращает названия полей, так же настраиваеться перевод
     * @return mixed
     */

    public function label(){

        $aData = [
            'ru' => [
                'Name'=>'Название',
                'Description'=>'Описание',
                'Budget'=>'Бюджет',
                'Start'=>'Начало',
                'Finish'=>'Окончание'],
            'en' => [
                'Name'=>'Name',
                'Description'=>'Description',
                'Budget'=>'Budget',
                'Start'=>'Start',
                'Finish'=>'Finish'],
        ];

        return $aData[Site::$data['lang']];
    }

    public function beforeSave(){
        $this->aFields['AuthorId'] = Site::$data['user']['id'];
        parent::beforeSave();
    }
} 