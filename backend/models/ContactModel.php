<?php

namespace backend\models;

use backend\library\Model;
use backend\library\Site;

class ContactModel extends Model {

    public function table(){
        return 'contact';
    }

    public function rules(){
        return [
            'required'=>['Data'],
            'required_by_type'=>[
                'phone'=>['int'=>['Data']],
                'email'=>['email'=>['Data']]],
            'enum'=>['Type'=>['phone','email']],
            'uniqum'=>['Data'],
            'fields'=>['UserId','Type','Data']
        ];
    }

    public function label(){
        $aData = [
            'ru' => [
                'UserId'=>'Пользователь',
                'Type'=>'Вид',
                'Data'=>'Контакт',
                'Phone'=>'Телефон',
                'Email'=>'Почта'],
            'en' => [
                'UserId'=>'User',
                'Type'=>'Type',
                'Data'=>'Data',
                'Phone'=>'Phone',
                'Email'=>'Email']
        ];

        return $aData[Site::$data['lang']];
    }

} 