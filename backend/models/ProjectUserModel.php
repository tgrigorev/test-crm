<?php

namespace backend\models;

use backend\library\Model;
use backend\library\Site;

class ProjectUserModel extends Model {
    public function table(){
        return 'project_user';
    }

    public function rules(){
        return [
            'required'=>['ProjectId','UserId','Start'],
            'int'=>['ProjectId','UserId'],
            'fields'=>['ProjectId','UserId','Start','Finish']
        ];
    }

    public function label(){

        $aData = [
            'ru' => [
                'ProjectId'=>'Проект',
                'UserId'=>'Пользователь',
                'Start'=>'Начало',
                'Finish'=>'Окончие'],
            'en' => [
                'ProjectId'=>'Project',
                'UserId'=>'User',
                'Start'=>'Start',
                'Finish'=>'Finish'],
        ];

        return $aData[Site::$data['lang']];
    }
} 