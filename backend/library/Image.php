<?php

namespace backend\library;


class Image {

    public $oFile;
    public $image_type;
    public $aSize;

    public function load($_oFile) {
        $this->aSize = getimagesize($_oFile);
        $this->image_type = $this->aSize[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->oFile = imagecreatefromjpeg($_oFile);
        } elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->oFile = imagecreatefromgif($_oFile);
        } elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->oFile = imagecreatefrompng($_oFile);
        }
    }

    public function save($_sFile, $_sPath = '/', $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null)
    {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->oFile, $_sPath.$_sFile.'.'.$this->type($image_type), $compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->oFile, $_sPath.$_sFile.'.'.$this->type($image_type));
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->oFile, $_sPath.$_sFile.'.'.$this->type($image_type));
        }
        if( $permissions != null) {
            chmod($_sFile,$permissions);
        }
    }
    public function output($image_type=IMAGETYPE_JPEG)
    {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->oFile);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->oFile);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->oFile);
        }
    }
    
    public function getWidth()
    {
        return imagesx($this->oFile);
    }

    public function getHeight()
    {
        return imagesy($this->oFile);
    }

    public function resizeToHeight($height)
    {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }

    public function resizeToWidth($width)
    {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }

    public function scale($scale)
    {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }

    public function resize($width,$height)
    {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->oFile , 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->oFile = $new_image;
    }

    public function type($_iType){
        $aTypes = ['','gif','jpg','png'];
        return $aTypes[$_iType];
    }
}