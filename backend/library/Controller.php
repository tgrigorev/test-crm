<?php

namespace backend\library;

use backend\library\Help;

abstract class Controller
{
    use Help;
    use Cache;

    abstract public function access();

    protected $css = ['main.css'];
    protected $js = ['ajax.js'];
    public $part = 'main';

    protected function view($_sPath,$aData=[])
    {
        $sPath = Site::$config['path'].'frontend/view/';

        if(!empty(Site::$error)){
            $sMessage = Site::$error;
            $sContent = $sPath.Site::$config['base']['controller'].'/'.Site::$config['base']['error'].'.php';
            $sPart = $sPath.'main.php';
        }else{
            foreach($aData as $sKey => $sValue) $$sKey = $sValue;

            $sContent = $sPath.Site::$data['controller'].'/'.$_sPath.'.php';
            $sPart = $sPath.$this->part.'.php';
        }

        ob_start();
        include($sContent);
        $content = ob_get_clean();

        ob_start();
        include($sPart);
        $content = ob_get_clean();

        include(Site::$config['path'].'frontend/view/layout.php');
    }

    protected function join($_sType)
    {
        $sData = '';
        $aType = [
            'css'=>['<link rel="stylesheet" href="/frontend/css/','">'],
            'js'=>['<script src="/frontend/js/','"></script>'],
            ];
        foreach($this->$_sType as $sAddress)
        {
            $sData .= $aType[$_sType][0]. $sAddress .$aType[$_sType][1];
        }
        return $sData;
    }
}