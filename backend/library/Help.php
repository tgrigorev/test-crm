<?php

namespace backend\library;


trait Help {

    public function redirect($_sAddress, $aParams = []){
        if($_sAddress === 'back')
            $sAddress = $_SERVER['HTTP_REFERER'];
        else
            $sAddress = $this->url($_sAddress, $aParams);

        header('Location:' . $sAddress);
    }

    public function url($_sAddress, $aParams = []){

        $sParams = '';

        foreach($aParams as $sName => $sValue){
            $sParams .= $sParams? '&'.$sName.'='.$sValue : '?'.$sName.'='.$sValue;
        }

        return $sAddress = ((Site::$data['lang'] == Site::$config['base']['lang']? '':'/'.Site::$data['lang']).
            $_sAddress.$sParams);
    }

    public function mail($_sAddress,$_sTitle,$_sMessage){
        $sHeaders   = array();
        $sHeaders[] = "MIME-Version: 1.0";
        $sHeaders[] = "Content-type: text/html; charset=UTF-8" . "\r\n";
        $sHeaders[] = "From: Sender Name <reception@test.com>";
        $sHeaders[] = "X-Mailer: PHP/".phpversion();

        return mail($_sAddress,$_sTitle,$_sMessage,implode("\r\n", $sHeaders));
    }
}