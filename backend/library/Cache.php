<?php
/**
 * Created by PhpStorm.
 * User: Spirit Fire
 * Date: 20.06.15
 * Time: 12:08
 */

namespace backend\library;


trait Cache {
    public static $mem;

    public static function Mem(){
        if(empty(self::$mem)){
            self::$mem = new \Memcache();
            self::$mem->connect('127.0.0.1', 11211);
        }
        return self::$mem;
    }

    public function cache_set($_sKey, $_sValue, $_tExpire = 0, $_sFlag = MEMCACHE_COMPRESSED){
        return self::Mem()->set($_sKey,$_sValue,$_sFlag,$_tExpire );
    }

    public function cache_get($_sKey){
        return self::Mem()->get($_sKey);
    }

    public function cache_delete($_mKey){
        return self::Mem()->get($_mKey);
    }
    public function cache_flush(){
        self::Mem()->flush();
    }
} 