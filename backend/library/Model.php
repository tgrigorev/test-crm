<?php

namespace backend\library;

abstract class Model
{
    use Help;
    use Cache;

    private static $oConnect;
    public $aFields;
    public $aErrors;
    public $aData;
    public $aPagination;

    abstract public function rules();
    abstract public function label();
    abstract public function table();

    public function __construct($_iId = NULL){
        if($_iId) $this->findById($_iId);
    }

    /**
     * Синглтон для доступа к базе данных
     * @return \mysqli объект для обращения к базе
     */

    public static function db()
    {
        $aConfig = Site::$config['db'];

        if (self::$oConnect == NULL) {
            self::$oConnect = mysqli_connect($aConfig['host'],$aConfig['user'],$aConfig['pass'],$aConfig['base']);
        }
        return self::$oConnect;
    }

    /**
     * Связи с другими таблицами
     * @return array условия связи
     */

    public function relation(){
        return [];
    }

    /**
     * Подготовка данных к сохранению
     */

    public function beforeSave(){}

    /**
     * Сохранение данных
     * @return bool|int|string результат сохранения id новой записи или false
     */

    public function save()
    {
        if($this->validate())
        {
            $this->beforeSave();
            $sFields = $sValues = '';
            foreach($this->rules()['fields'] as $sField){
                if(!empty($this->aFields[$sField])){
                    $sFields .= empty($sFields)? "`".$sField."`":", `".$sField."`";
                    $sValues .= empty($sValues)? "'".$this->aFields[$sField]."'":", '".$this->aFields[$sField]."'";
                }
            }

            $sSql = 'INSERT INTO '.$this->table().' ('.$sFields.') VALUES ('.$sValues.')';

            return $this->aFields['Id'] = self::execute($sSql);
        }
        return false;
    }

    /**
     * Подготовка данных к проверке, блокирует повторное применение действий к данным
     */

    public function beforeValidate(){}

    /**
     * Проверка даннык к соответствию и сохранение ошибок в public переменную aErrors
     * @param array $_aFields поля для проверки
     * @return bool результат проверки true или false
     */

    public function validate( $_aFields = [] )
    {
        $this->beforeValidate();

        $aError = [];
        $aRules = $this->rules();

        foreach( $aRules as $sRule => $aRule )
        {
            if(!empty($_aFields)){
                $aResult = array_intersect($_aFields,$aRule);
                if(!empty($aResult)) continue;
            }

            $aError = array_merge($aError, $this->check($sRule,$aRule));
        }
        $this->aErrors = $aError;
        return count($aError)?false:true;
    }

    /**
     * Проверка даннык по правилам
     * @param $_sType условия проверки
     * @param $_aData данные для проверки
     * @return array результат, возвращение ошибок
     */

    public function check($_sType, $_aData){
        $aError = [];
        $aLabels = $this->label();

        switch($_sType){
            case 'required':
                foreach($_aData as $sField)
                {
                    if(empty($this->aFields[$sField]) && empty($this->aData))
                        $aError[$sField] = 'Заполните поле';
                }
                break;
            case 'required_by_type':
                if(array_key_exists($this->aFields['Type'],$_aData)){
                    foreach($_aData[$this->aFields['Type']] as $sRule => $aRule)
                        $aError = $this->check($sRule, $aRule);
                }
                break;
            case 'enum':
                foreach($_aData as $sField => $aRule)
                {
                    if(!in_array($this->aFields[$sField],$aRule))
                        $aError[$sField] = 'Выбирете предложенный вариант';//.$aLabels[$sField];
                }
                break;
            case 'uniqum':
                foreach($_aData as $sField)
                {
                    if(!empty($this->aFields[$sField]) && empty($this->aData) &&
                            $this->exists([$sField => $this->aFields[$sField]]))
                    {
                        $aError[$sField] = 'Похожий '.$aLabels[$sField].' уже существует, выберите себе другой.';
                    }
                }
                break;
            case 'string':
                foreach($_aData as $sField)
                {
                    $sPreg = '/^[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя\s\-\w\-\!\,\.\/]*$/msi';
                    if(!empty($this->aFields[$sField]) && !preg_match($sPreg, $this->aFields[$sField]))
                        $aError[$sField] = 'Вы использовали недопустимые символы.';
                }
                break;
            case 'confirm':
                if(!empty($this->aFields[$_aData[0]]) && $this->aFields[$_aData[0]]!=$this->aFields[$_aData[1]]
                        && empty($aError[$_aData[0]]))
                    $aError[$_aData[0]] = 'Пароли не совпадают';
                break;
            case 'int':
                foreach($_aData as $sField)
                {
                    if(!(empty($this->aFields[$sField]) || $this->aFields[$sField]>0))
                        $aError[$sField] = 'В поле - ' . $aLabels[$sField] . ' необходимо указать число';
                }
                break;
            case 'date':
                foreach($_aData as $sField)
                {
                    $dBirth = $this->aFields[$sField];
                    if(is_array($dBirth)){
                        if(!($dBirth['Month'] && $dBirth['Day'] && $dBirth['Year'])){
                            $aError[$sField] = 'Заполните все поля';
                        }elseif(checkdate($dBirth['Month'], $dBirth['Day'], $dBirth['Year']))
                        {
                            if((date('Y') - $dBirth['Year'])<18){
                                $aError[$sField] = 'К сожалению вы очень юны, попробуйте когда вам исполниться 18';
                            }elseif((date('Y') - $dBirth['Year'])>100){
                                $aError[$sField] = 'К сожалению вам очень много лет';
                            }
                        }else{
                            $aError[$sField] = 'В поле - ' . $aLabels[$sField] . ' ошибка';
                        }
                        $this->aFields[$sField] = $dBirth['Year'].'-'.$dBirth['Month'].'-'.$dBirth['Day'];
                    }
                }
                break;
            case 'email':
                foreach($_aData as $sField)
                {
                    if(!filter_var($this->aFields[$sField], FILTER_VALIDATE_EMAIL))
                        $aError[$sField] = 'Вы указали неверный email';
                }
                break;
            default: return [];
        }
        return $aError;
    }

    /**
     * Присвоение параметров полученных от пользователя
     * @param $_aDataPost
     */

    public function attributes($_aDataPost)
    {
        foreach($this->rules()['fields'] as $sField){
            if(array_key_exists($sField,$_aDataPost)){
                $this->aFields[$sField] = $_aDataPost[$sField];
            }
        }
    }

    /**
     * Выполнение запроса к базе, типа UPDATE, INSERT
     * @param $_sSql
     * @return int|string
     */

    public static function execute($_sSql)
    {
        $oSqlI = self::db();

        mysqli_query( $oSqlI, $_sSql );

        return mysqli_insert_id($oSqlI);
    }

    /**
     * Выполнение запроса к базе, для извлечения одной записи
     * @param $_sSql
     * @return array|null
     */

    public static function query($_sSql)
    {
        $oSqlI = self::db();
        $oRequest = mysqli_query( $oSqlI, $_sSql );
        return mysqli_fetch_assoc( $oRequest );
    }

    /**
     * Выполнение запроса к базе, для извлечения группы записей
     * @param $_sSql Строка запроса
     * @return array Результат
     */

    public static function query_all($_sSql)
    {
        $aData =[];
        $oSqlI = self::db();
        $oRequest = mysqli_query( $oSqlI, $_sSql );
        while ($row = mysqli_fetch_assoc( $oRequest )) {
            $aData[] = $row;
        }

        return $aData;
    }

    /**
     * Обновление данных
     * @return int|string Результат выполнения
     */

    public function update(){

        $this->beforeSave();

        $sFields = '';

        foreach($this->rules()['fields'] as $sField)
        {
            if(empty($this->aFields[$sField])) continue;
            $sFields .= empty($sFields)? $sField.' = "'.$this->aFields[$sField].'"':', '.$sField.' = "'.$this->aFields[$sField].'"';
        }
        $sSql = 'UPDATE '.$this->table().' SET '.$sFields.' WHERE Id = '.$this->aData['Id'].' LIMIT 1';

        return self::execute($sSql);
    }

    /**
     * Извлечения одной записи из базы данных, по запросу
     * @param $sSql Строка запросса
     * @return $this Текущий объект с внесенными данными запроса в public переменную aData
     */

    public function findBySql($sSql)
    {
        $this->aData = self::query($sSql);

        return $this;
    }

    /**
     * Извлечения группы записей из базы данных, по запросу
     * @param $sSql Строка запросса
     * @return $this Текущий объект с внесенными данными запроса в public переменную aData
     */

    public function findAllBySql($sSql)
    {
        $this->aData = self::query_all($sSql);

        return $this;
    }

    /**
     * Извлечение записи по id
     * @param $_iId id троки
     * @return $this|bool Текущий объект с внесенными данными запроса в public переменную aData
     */

    public function findById($_iId)
    {
        if(!empty($_iId) && is_numeric($_iId))
        {
            $sSql = 'SELECT * FROM '.$this->table().' WHERE Id = '. $_iId.' LIMIT 1';

            if($oResult = self::query($sSql)){
                $this->aFields=$this->aData = $oResult;
            }

            return $this;
        }

        return false;
    }

    /**
     * Проверка существования записи
     * @param $_aData массив условий
     * @return mixed результат существования
     */

    public function exists($_aData)
    {
        $sWhere = '';

        foreach($_aData as $sField => $sData){
            $sWhere .= empty($sWhere)? $sField.' = "'.$sData.'"':' AND '.$sField.' = "'.$sData.'"';
        }

        $sSql = 'SELECT EXISTS( SELECT Id FROM '.$this->table().' WHERE '.$sWhere.') `exists`';

        return self::query($sSql)['exists'];
    }

    /**
     * Механизм извлечения одной записи по параметрам
     * @param $_aData условия поиска
     * @param array $_aParam дополнительные условия и настройки запроса
     * @return $this Текущий объект с внесенными данными запроса в public переменную aData
     */

    public function findByParams($_aData,$_aParam=[])
    {
        $sWhere = '';

        foreach($_aData as $sField => $sData){
            $sData = htmlspecialchars($sData);
            $sWhere .= empty($sWhere)? $sField.' = "'.$sData.'"':' AND '.$sField.' = "'.$sData.'"';
        }

        if(!empty($_aParam['fields'])){
            $sFields = implode(',',$_aParam['fields']);
        }else $sFields='*';

        $sSql = 'SELECT '.$sFields.' FROM '.$this->table().' WHERE '.$sWhere.' LIMIT 1';

        if($oResult = self::query($sSql)){
            $this->aFields=$this->aData = $oResult;
        }
        return $this;
    }

    /**
     * Механизм извлечения группы записей по параметрам
     * @param $_aData условия поиска
     * @param array $_aParam дополнительные условия и настройки запроса
     * @return $this Текущий объект с внесенными данными запроса в public переменную aData
     */

    public function findAllByParams($_aData,$_aParam=[])
    {
        $sWhere = '';
        $sJoin  = '';
        $sFields = '';

        foreach($_aData as $sField => $sData){

            if(is_array($sData)){
                $sWhereArray = '';
                foreach($sData as $sDataArray){
                    $sDataArray = htmlspecialchars($sDataArray);
                    $sWhereArray .= empty($sWhereArray)? $sField.' = "'.$sDataArray.'"':' OR '.$sField.' = "'.$sDataArray.'"';
                }
                $sWhere .= $sWhere?' AND '.$sWhereArray:''.$sWhereArray;
            }else{
                $sData = htmlspecialchars($sData);
                if(!empty($_aParam['type']) && $_aParam['type'] == 'like'){
                    $sWhere .= empty($sWhere)? ' '.$sField.' LIKE "%'.$sData.'%"':' AND '.$sField.' LIKE "%'.$sData.'%"';
                }elseif($sData == 'NULL'||$sData == 'NOT NULL'){
                    $sWhere .= empty($sWhere)? ' '.$sField.' IS '.$sData:' AND '.$sField.' IS '.$sData;
                }else{
                    $sWhere .= empty($sWhere)? $sField.' = "'.$sData.'"':' AND '.$sField.' = "'.$sData.'"';
                }
            }
        }

        if(!empty($_aParam['fields'])){
            foreach($_aParam['fields'] as $sField){
                $sFields .= empty($sFields) ? 'main.'.$sField : ', main.'.$sField;
            }
        }else $sFields='main.*';

        if(!empty($_aParam['pagination'])){
            $sPagination = 'LIMIT '.$_aParam['pagination'][0]*$_aParam['pagination'][1].
                ','.$_aParam['pagination'][1];
        }else $sPagination='';

        if(!empty($_aParam['limit'])){
            $sLike = 'LIMIT '.$_aParam['limit'];
        }else{
            $sLike = '';
        }

        if(!empty($_aParam['join'])){
            if(!is_array($_aParam['join']))$_aParam['join'] = array_keys($this->relation());
            foreach($_aParam['join'] as $sName){
                $aData = $this->relation()[$sName];
                $sFields .= ', '.$sName.'.'.$aData[2].' '.$sName;
                $aData[0] = 'backend\models\\'.$aData[0];
                $sJoin .= ' LEFT JOIN '.$aData[0]::table().' '.$sName.' ON '.$sName.'.'.$aData[1][0].
                    ' = main.'.$aData[1][1];
                if(!empty($aData[3]))
                {
                    foreach($aData[3] as $sFieldParam => $sParam){
                        $sJoin .=' AND '.$sName.'.'.$sFieldParam.' = "'.$sParam.'"';
                    }
                }
            }
        }

        $sSql = 'SELECT '.$sFields.' FROM '.$this->table().' main '.$sJoin.' WHERE '.$sWhere.' '.$sPagination.' '.$sLike;

        if($aoResult = self::query_all($sSql)){
            $this->aFields=$this->aData = $aoResult;

            if(!empty($_aParam['pagination'])) {
                $this->aPagination['count'] = $this->count($_aData);
                $this->aPagination['on_page'] = $_aParam['pagination'][1];
                $this->aPagination['page'] = $_aParam['pagination'][0];
            }
        }
        return $this;
    }

    /**
     * Извлечение количества строк по условию
     * @param $_aData условия поиска
     * @return mixed количество строк
     */

    public function count($_aData){
        $sWhere = '';

        foreach($_aData as $sField => $sData){
            $sWhere .= empty($sWhere)? $sField.' = "'.$sData.'"':' AND '.$sField.' = "'.$sData.'"';
        }

        $sSql = 'SELECT COUNT(Id) `count` FROM '.$this->table().' WHERE '.$sWhere;

        return self::query($sSql)['count'];
    }

    /**
     * Удаление записи по параметрам
     * @param $_aData параметры
     * @return int|string результат удаления
     */

    public function delete($_aData){
        $sWhere = '';

        foreach($_aData as $sField => $sData){
            $sData = htmlspecialchars($sData);
            $sWhere .= empty($sWhere)? $sField.' = "'.$sData.'"':' AND '.$sField.' = "'.$sData.'"';
        }

        $sSql = 'DELETE FROM '.$this->table().' WHERE '.$sWhere;

        return self::execute($sSql);
    }
}