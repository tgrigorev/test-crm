<?php

namespace backend\library;

use backend\models\Login;
use backend\library\Help;


class Site {

    use Help;

    public static $config;
    public static $error;
    public static $data;
    public static $lang = 'ru';

    public function run($config)
    {
        self::$config = $config;

        $aRouter = $this->router();

        $oController = new $aRouter['controller']();

        $sAccess = $this->access($oController,$aRouter['action']);

        $aRouter['action'] .= 'Action';

        if($sAccess === 'login') $this->redirect('/login');
        elseif($sAccess === 'error') self::$error = 'У вас нет доступа к этой странице';

        $oController->$aRouter['action']();

    }

    private function router()
    {
        $sRequest = explode('?',$_SERVER['REQUEST_URI']);

        self::$data['url'] = $sRequest[0];

        $aUrl = explode('/',substr($sRequest[0],1));

        $_aRouter = $aRouter = [
            'controller' => 'backend\controllers\\'.ucfirst(self::$config['base']['controller']).'Controller',
            'action' => self::$config['base']['action'],
        ];

        if($aUrl[0] && in_array($aUrl[0], self::$config['lang'])){
            self::$data['lang'] = array_shift($aUrl);
        }else{
            self::$data['lang'] = self::$config['base']['lang'];
        }

        if($aUrl[0])
        {
            foreach(self::$config['router'] as $aRoutes)
            {
                $aRoute = explode('/',$aRoutes[1]);

                if(count($aUrl) === count($aRoute))
                {
                    $aData = [];
                    $aPreg = explode('/',$aRoutes[0]);

                    foreach($aPreg as $iKey => $sPreg)
                    {
                        if(preg_match('@^'.$sPreg.'$@', $aUrl[$iKey]))
                        {
                            $aData[$aRoute[$iKey]] = $aUrl[$iKey];
                        }
                        else break;
                    }

                    $sController = empty($aData['controller'])?self::$config['base']['controller']:$aData['controller'];

                    $_aRouter['controller'] ='backend\controllers\\'.ucfirst($sController).'Controller';

                    $sAction = (empty($aData['action'])?self::$config['base']['action']:
                        $aData['action']);

                    $_aRouter['action'] = $sAction;

                    $sControllerFile = __DIR__.'/../../'.$_aRouter['controller'].'.php';

                    if(file_exists($sControllerFile) && method_exists($_aRouter['controller'],$_aRouter['action'].'Action'))
                    {
                        self::$data['controller'] = $sController;
                        self::$data['action'] = $sAction;
                        break;
                    }else{
                        $_aRouter['controller'] = 'backend\controllers\\'.self::$config['base']['controller'].'Controller';
                        self::$data['controller'] = self::$config['base']['controller'];
                        self::$data['action'] = $_aRouter['action'] = self::$config['base']['error'];
                    }
                }
            }
            $aRouter = $_aRouter;
        }

        return $aRouter;
    }

    private function access($_oController, $sAction){
        $sResult = '';
        $aAccess = $_oController->access();
        $aUserAccess = Login::check();

        if(array_key_exists('*',$aAccess) &&
            (in_array('*',$aAccess['*']) || in_array('*',$sAction)))
                    return true;

        if(!empty($aUserAccess['role']) &&
            array_key_exists($aUserAccess['role'],$aAccess) &&
            (in_array('*', $aAccess[$aUserAccess['role']]) ||
                in_array($sAction, $aAccess[$aUserAccess['role']])))
                    return true;

        if(empty($aUserAccess['role']) &&
            !array_key_exists('*',$aAccess))
                    $sResult = 'login';

        elseif(!empty($aUserAccess['role']) &&
            (!(array_key_exists('*',$aAccess) && (in_array('*', $aAccess['*']) || in_array($sAction, $aAccess['*'])))
            ||
            !(array_key_exists($aUserAccess['role'],$aAccess) &&
                (in_array('*', $aAccess[$aUserAccess['role']]) ||
                    in_array($sAction, $aAccess[$aUserAccess['role']])))))
                        $sResult = 'error';

        return $sResult;
    }
}