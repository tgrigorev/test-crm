<?php

namespace backend\controllers;

use backend\library\Site;
use backend\library\Controller;
use backend\models\ContactModel;
use backend\models\InfoModel;
use backend\models\Login;
use backend\models\PositionModel;
use backend\models\UserModel;

class CabinetController extends Controller {

    public $part = 'cabinet';

    /**
     * Настройка доступа к страницам
     * @return array access
     */
    public function access(){
        return [
            'admin'=>['*'],
            'client'=>['index','edit'],
        ];
    }

    /**
     * Основная страница кабинета пользователя
     */
    public function indexAction()
    {
        $oUser = new UserModel();

        $oUser->findById(Site::$data['user']['id']);

        $this->view('index', compact('oUser'));
    }

    /**
     * Клиенты компании
     */

    public function clientsAction(){
        $oUsers = new UserModel();
        $oContact = new ContactModel();

        $oUsers->findAllByParams(['Role'=>'client'],['fields'=>['Id','FIO'],'join'=>['email','phone','city'],
            'pagination'=>[$_GET['page']?$_GET['page']:0,10]]);

        $this->view('clients',compact('oUsers','oContact'));
    }

    /**
     * Редактирование данных
     */
    public function editAction(){
        if(Site::$data['user']['role'] == 'client' && Site::$data['user']['id'] !== $_GET['id'])
            $this->redirect('/cabinet');

        $oUser = (new UserModel())->findByParams(['Id'=>$_GET['id']]);
        $oPhone = (new ContactModel())->findByParams(['Type'=>'phone','UserId'=>$_GET['id']]);
        $oEmail = (new ContactModel())->findByParams(['Type'=>'email','UserId'=>$_GET['id']]);

        if($_POST['register']){
            $oUser->attributes($_POST['register']);
            $oUser->aFields['PasswordConfirm']=$_POST['register']['PasswordConfirm'];

            $oPhone->attributes($_POST['register']['Phone']);
            $oPhone->aFields['Type'] = 'phone';

            $oEmail->attributes($_POST['register']['Email']);
            $oEmail->aFields['Type'] = 'email';

            if(Site::$data['user']['role'] != 'admin') unset($oUser->aFields['Role']);

            if($oUser->validate()&
                $oPhone->validate()&
                $oEmail->validate())
            {
                $oUser->update();
                $oPhone->aFields['UserId'] = $oEmail->aFields['UserId'] =
                    $oUser->aData['Id'];
                $oPhone->update();
                $oEmail->update();

                $this->redirect('/cabinet');
            }
        }

        $this->view('register',compact('oUser','oPhone','oEmail','oImage','oInfo'));
    }

    /**
     * Клиенты компании
     */
    public function deleteAction(){
        (new ContactModel())->delete(['UserId'=>$_GET['id']]);
        (new InfoModel())->delete(['UserId'=>$_GET['id']]);
        (new UserModel())->delete(['Id'=>$_GET['id']]);

        $this->redirect('/cabinet/clients');
    }

    public function registerAction(){

        $oUser = new UserModel();
        $oPhone = new ContactModel();
        $oEmail = new ContactModel();

        if($_POST['register']){
            $oUser->attributes($_POST['register']);
            $oUser->aFields['PasswordConfirm'] = $_POST['register']['PasswordConfirm'];

            $oPhone->attributes($_POST['register']['Phone']);
            $oPhone->aFields['Type'] = 'phone';

            $oEmail->attributes($_POST['register']['Email']);
            $oEmail->aFields['Type'] = 'email';

            if(Site::$data['user']['role'] != 'admin') unset($oUser->aFields['Role']);

            if($oUser->validate()&
                $oPhone->validate()&
                $oEmail->validate())
            {
                $oUser->sendMailRegister($oEmail->aFields['Data'],
                        ['login'=>$oUser->aFields['Login'],'password'=>'Password']);
                if($oUser->save()){
                    $oPhone->aFields['UserId'] = $oEmail->aFields['UserId'] = /*$oImage->aFields['UserId'] =*/
                        $oUser->aFields['Id'];
                    $oPhone->save();
                    $oEmail->save();

                    $this->redirect('/cabinet');
                }

            }
        }

        $this->view('register',compact('oUser','oPhone','oEmail','oImage','oInfo','oPosition'));
    }

    public function positionAction(){
        if(empty($_POST['ajax'])) $this->redirect('/cabinet');

        $oPosition = new PositionModel();
        $aResult = $oPosition->getPosition([$_POST['name'],$_POST['country']],$_POST['type']);

        echo json_encode($aResult);
    }
}