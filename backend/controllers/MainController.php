<?php

namespace backend\controllers;

use backend\library\Site;
use backend\library\Controller;
use backend\models\ContactModel;
use backend\models\ImageModel;
use backend\models\InfoModel;
use backend\models\Login;
use backend\models\PositionModel;
use backend\models\UserModel;


class MainController extends Controller
{

    public function access(){
        return [
            '*' => ['*'],
        ];
    }

    public function indexAction(){
        $this->redirect('/login');
        $this->view('index');
    }

    public function changeAction(){
        if(!empty(Site::$data['user'])) $this->redirect('/cabinet');
        if(empty($_GET['key']) || !$this->cache_get($_GET['key'])) $this->redirect('/login');

        $sError = NULL;
        $iDone = 0;

        $oUser = new UserModel();

        if(!empty($_POST['ajax']))
        {
            if(empty($_POST['Password'])){
                $sError = 'no_password_error';
            }elseif(!empty($_POST['Password']) && empty($_POST['PasswordConfirm'])){
                $sError = 'no_password_confirm_error';
            }elseif($_POST['Password'] !== $_POST['PasswordConfirm']){
                $sError = 'password_no_confirm_error';
            }else{
                $oUser->findById($this->cache_get($_GET['key']));
                if(!empty($oUser->aData)){
                    $oUser->aFields['Password'] = Login::hash($_POST['Password']);

                    $oUser->update();

                    $this->cache_delete($_GET['key']);

                    $aContact = $oUser->getContact();

                    $iDone = $oUser->sendMailChanged($aContact->aData['Data'],$_POST['Password'])?1:0;
                }else{
                    $sError = 'login_error';
                }
            }

            echo json_encode(['error'=>$sError,'done'=>$iDone]);

            return;
        }

        $this->view('change',compact('oUser'));
    }

    public function lostAction(){
        if(!empty(Site::$data['user'])) $this->redirect('/cabinet');

        $sError = NULL;
        $iSend = 0;

        $oUser = new UserModel();

        if(!empty($_POST['ajax']))
        {
            if(empty($_POST['Login'])){
                $sError = 'no_login_error';
            }else{
                $oUser->findByParams(['Login'=>$_POST['Login']],['Id']);
                if(!empty($oUser->aData)){
                    $aContact = $oUser->getContact();

                    $sProtect = sha1(($aContact->aData['Data']).date('U'));
                        $this->cache_set($sProtect,$oUser->aData['Id'],(60*60*24));

                    $iSend = $oUser->sendMailChange($aContact->aData['Data'],$sProtect)?1:0;
                }else{
                    $sError = 'login_error';
                }
            }

            echo json_encode(['sProtect'=>$sProtect,'error'=>$sError,'send'=>$iSend]);

            return;
        }

        $this->view('login',compact('oUser'));
    }

    public function loginAction(){
        if(!empty(Site::$data['user'])) $this->redirect('/cabinet');

        $oUser = new UserModel();
        $iAuth = $iPass = 0;

        if(!empty($_POST['ajax'])){

            if(empty($_POST['Login']) && !isset($_POST['Password'])){
                if(empty($oUser->aData)) $oUser->aErrors['main'] = 'no_login_error';
            }elseif(!empty($_POST['Login']) && !isset($_POST['Password'])){
                if($oUser->exists(['Login'=>$_POST['Login']])){
                    $iPass = 1;
                }else{
                    $oUser->aErrors['main'] = 'login_error';
                }
            }elseif(!empty($_POST['Password']) && Login::login($_POST['Login'],$_POST['Password'],$oUser)){
                $iAuth = 1;
            }else{
                $oUser->aErrors['main'] = 'password_error';
            }

            echo json_encode(['error'=>$oUser->aErrors['main'],'pass'=>$iPass,'auth'=>$iAuth]);

            return;
        }

        $this->view('login',compact('oUser'));
    }

    public function logoutAction()
    {
        Login::logout();

        $this->redirect('/');
    }

    public function errorAction(){
        $sMessage = 'Страница не найдена!';
        $this->view('error',compact('sMessage'));
    }
}