<?php

namespace backend\controllers;

use backend\library\Site;
use backend\library\Controller;
use backend\models\ProjectModel;
use backend\models\ProjectUserModel;
use backend\models\TaskModel;

class ProjectController extends Controller {

    public $part = 'cabinet';

    /**
     * Настройка доступа к страницам
     * @return array access
     */
    public function access(){
        return [
            'admin'=>['*'],
            'client'=>['index','archive','add','edit'],
        ];
    }
    /**
     * Все проекты
     */
    public function indexAction()
    {
        $aParam = [];
        $oProject = new ProjectModel();

        if(Site::$data['user']['role'] == 'client') $aParam = ['AuthorId'=>Site::$data['user']['id']];

        $oProject->findAllByParams(array_merge(['Finish'=>'NULL'],$aParam));

        $this->view('index', compact('oProject'));
    }
    /**
     * Архив проектов
     */
    public function archiveAction()
    {
        $aParam = [];
        $oProject = new ProjectModel();

        if(Site::$data['user']['role'] == 'client') $aParam = ['AuthorId'=>Site::$data['user']['id']];

        $oProject->findAllByParams(array_merge(['Finish'=>'NOT NULL'],$aParam));

        $this->view('index', compact('oProject'));
    }
    /**
     * Добавление проекта
     */
    public function addAction()
    {
        $oProject = new ProjectModel();

        if(!empty($_POST['register'])){

            $oProject->attributes($_POST['register']);
            $oProject->aFields['AuthorId'] = Site::$data['user']['id'];
            $oProject->aFields['Start'] = date("Y-m-d H:i:s");

            if($oProject->validate() && $oProject->save())
            {
                $this->redirect('/project');
            }
        }

        $this->view('form', compact('oProject'));
    }
    /**
     * Редактирование проекта
     */
    public function doneAction()
    {
        $oProject = new ProjectModel();

        $oProject->findById($_GET['id']);

        $oProject->aFields['Finish'] = date("Y-m-d H:i:s");

        if($oProject->validate())
        {
            $oProject->update();
            $this->redirect('/project');
        }

        $this->redirect('back');
    }

    public function editAction()
    {
        $oProject = new ProjectModel();

        $oProject->findById($_GET['id']);

        if(Site::$data['user']['role'] != 'admin' && !empty($oProject->aData) &&
                Site::$data['user']['id'] !== $oProject->aData['AuthorId'])
            $this->redirect('/project');

        if(!empty($_POST['register'])){

            $oProject->attributes($_POST['register']);

            if($oProject->validate())
            {
                $oProject->update();
                $this->redirect('/project');
            }
        }

        $this->view('form', compact('oProject'));
    }
    /**
     * Удаление проекта
     */
    public function deleteAction()
    {
        if(Site::$data['user']['role'] != 'admin') $this->redirect('/project');

        $oProject = new ProjectModel();
        $oProjectUser = new ProjectUserModel();
        $oTask = new TaskModel();

        $oProjectUser->delete(['ProjectId'=>$_GET['id']]);
        $oTask->delete(['ProjectId'=>$_GET['id']]);
        $oProject->delete(['Id'=>$_GET['id']]);

        $this->redirect('/project');
    }

    /**
     * Подключение и отстронение персонала от проекта
     */
    public function userAction()
    {
        $this->view('user');
    }
} 