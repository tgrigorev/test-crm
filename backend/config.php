<?php

return [
    'name'=>'Тестовое приложение',
    'path'=>__DIR__.'/../',
    'base'=>[
        'controller'=>'main',
        'action'=>'index',
        'error'=>'error',
        'lang'=>'ru',
    ],
    'db'=>[
        'host'=>"localhost",
        'user'=>"test",
        'pass'=>"123",
        'base'=>"test_db"
    ],
    'router'=>[
        ['\w+/\w+','controller/action'],
        ['\w+','controller'],
        ['\w+','action'],
    ],
    'load'=>[
        'Controller'
    ],
    'image'=>[
        'path'=>'frontend/images/',
        'url'=>'/frontend/images/',
    ],
    'lang'=>['ru','en'],
    'user'=>[
        'role'=>''
    ]
];